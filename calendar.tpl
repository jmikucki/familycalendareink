<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <link rel="icon" type="image/png" sizes="32x32" href="/static_files/favicon-32x32.png" />
  <link rel="icon" type="image/png" sizes="16x16" href="/static_files/favicon-16x16.png" />
  <script src="https://cdn.tailwindcss.com"></script>
  <title>Site title goes here</title>
  <link rel="stylesheet" href="fonts.css">
</head>
<body>
    <div class='h-[{{display['width']}}px] bg-[{{display['color_bg']}}] font-[unscii2] text-[16px]'>
        <div class='flex flex-col h-full'>
            <div class='flex flex-row justify-between bg-[{{display['color_notice']}}] border-y-2 border-solid border-black h-auto'>
                <p class='p-1'>Kalendarz rodzinny</p>
                <p class='p-1'>{{current_date}}</p>
            </div>

            <div class='flex flex-col px-2 py-1 h-auto'>
                {% for task in tasks_by_day[0] %}
                <div class='flex flex-row bg-[{{display['color_error'] if task.description.startswith('!') else display['color_bg']}}] m-1 divide-x-2 divide-black border-2 border-solid border-black rounded-md'>
                    <p class='px-2 py-1'>{{task.from}}</p>
                    <p class='px-1 py-1 break-words'>{{task.description.lstrip('!')}}</p>
                </div>
                {% endfor %}
            </div>
            {% if show_days_after + show_days_before > 0 %}
            <div class='flex flex-row justify-evenly border-t-2 border-black divide-x-2 divide-black h-full'>
                {% for date_offset in range(-show_days_before, show_days_after+1) %}
                {% if date_offset != 0 %}
                <div class='flex flex-col w-full'>
                    <div class='flex border-b-2 border-black text-center'>
                        <p class='bg-[{{display['color_notice']}}] w-full'>{{days_names[(current_day_nr+date_offset)%7]}}</p>
                    </div>

                    {% for task in tasks_by_day[show_days_before+date_offset] %}
                    <div class='flex flex-row bg-[{{display['color_error'] if task.description.startswith('!') else display['color_bg']}}] m-1 divide-x-2 divide-black border-2 border-solid border-black rounded-md'>
                        <p class='px-2 py-1'>{{task.from}}</p>
                        <p class='px-1 py-1 break-words'>{{task.description.lstrip('!')}}</p>
                    </div>
                    {% endfor %}
                </div>
                {% endif %}
                {% endfor %}
            </div>
            {% endif %}
        </div>
    </div>
</body>