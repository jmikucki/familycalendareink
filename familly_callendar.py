import sys
import os

import logging
import socket
import jinja2
from html2image import Html2Image

from icalevents.icalevents import ICalDownload
from icalevents.icalparser import parse_events

import time
import datetime
from dateutil.tz import UTC
from PIL import Image, ImageColor, ImageOps

try:
    libdir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'lib')
    if os.path.exists(libdir):
        sys.path.append(libdir)
    from waveshare_epd import epd2in7b,epd7in3g
    supportForEink = True
except OSError as e:
    logging.info("Failed to import eink driver")
    supportForEink = False


configurations_display = {
    "eink_7in3":
    {
        "type": "eink_7in3",
        "width": 480,
        "height": 800,
        "split_colors": False,
        'color_text': r'#000000',
        'color_notice': r'#FFFF00',
        'color_error': r'#FF0000',
        'color_bg': r'#FFFFFF',
    },

    "eink_2in7":
    {
        "type": "eink_2in7",
        "width": 176,
        "height": 264,
        "split_colors": True,
        'color_text': r'#000000',
        'color_notice': r'#FF0000',
        'color_error': r'#FFFF00',
        'color_bg': r'#FFFFFF',
    },

    "png":
    {
        "type": "png",
        "width": 480,
        "height": 800,
        "split_colors": False,
        'color_text': r'#000000',
        'color_notice': r'#FFFF00',
        'color_error': r'#FF0000',
        'color_bg': r'#FFFFFF',
    }
}

configuration = {
    'days_names':
    [
        'Poniedziałek',
        'Wtorek',
        'Środa',
        'Czwartek',
        'Piątek',
        'Sobota',
        'Niedziela',
    ],
    'refresh_minutes_min': 30,
    'refresh_minutes_max': 600,
    'show_days_before':0,
    'show_days_after': 3,
}

SCREENSHOT_FILENAME='screenshot.bmp'

ICAL_URL=r'https://calendar.google.com/calendar/ical/g5g8i25pprmujsngn3l7ullslo%40group.calendar.google.com/private-11629aba8879a20c64aa9741db754253/basic.ics'


def check_internet(host="8.8.8.8", port=53, timeout=3):
    """
    Host: 8.8.8.8 (google-public-dns-a.google.com)
    OpenPort: 53/tcp
    Service: domain (DNS/TCP)
    """
    try:
        socket.setdefaulttimeout(timeout)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        return True
    except socket.error as ex:
        print(ex)
        return False
    
def download_calendar_data(ical_link, configuration):
    ical = ICalDownload()
    try:
        data = ical.data_from_url(ICAL_URL)
        now = datetime.datetime.now()

        events = parse_events(data,
            start=now-datetime.timedelta(days=configuration['show_days_before']),
            end=now+datetime.timedelta(days=configuration['show_days_after']))
    except:
        logging.info('Failed to download ical data.')
        return None
    return events

def process_calendar_data(calendar_data, configuration):
    tasks_by_day = {}
    today = datetime.datetime.now().date()
    for day_nr in range(-configuration['show_days_before'],configuration['show_days_after']+1):
        logging.info(f'Processing day: {day_nr}')
        tasks_by_day[day_nr] = []
        for event in calendar_data:
            if event.start.date() == (today+datetime.timedelta(days=day_nr)):
                tasks_by_day[day_nr].append({'from': event.start.time().strftime('%H:%M'), 'to': event.end.time(), 'time': f'''{event.start.time().isoformat('minutes')}-{event.end.time().isoformat('minutes')}''', 'description':event.summary})
                logging.info('{} - {}: {}'.format(day_nr, event.start.time(), event.summary))
        tasks_by_day[day_nr].sort(key=(lambda e: e['from']))
    return tasks_by_day

def generate_html(calendar_data, configuration):
    jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader("./"))
    template = jinja_env.get_template("calendar.tpl")
    calendar_data.update(configuration)

    logging.info(f'Data for rendering: {calendar_data}')
    content = template.render({**calendar_data, **configuration})

    with open('output.html', 'wb') as f:
        f.write(content.encode('utf-8'))

    return content

def generate_rgb_image(html_data, other_files_paths, config):
    custom_flags=[
        '--disable-composited-antialiasing',
        '--disable-font-subpixel-positioning',
        '--default-background-color=00000000',
        '--hide-scrollbars',
        '--no-sandbox',
        '--headless',
        '--disable-gpu'
    ]

    browser = 'chrome' #, 'firefox'
    hti = Html2Image(browser=browser, size=(config['display']['height'], config['display']['width']), custom_flags=custom_flags)

    # other_files = [p for p,_ in other_files_data]
    for path in other_files_paths:
        hti.load_file(src=path, as_filename=path)
    
    hti.load_str(html_data, as_filename='index.html')

    # # Use chrome to generate an image of the page
    image_path = 'screenshot.png'
    hti.screenshot_loaded_file('index.html', output_file=image_path, size=(config['display']['height'], config['display']['width']))
    img = Image.open(image_path)
    img = img.convert('RGB')

    return img

def split_image_by_color(image_rgb, colors = []):
    images_by_color = []
    colors_in_image=set()
    for color in colors:
        logging.info(f"Look for a color: {color}")
        new_image = image_rgb.convert(mode='1')
        for y in range(image_rgb.height):
            for x in range(image_rgb.width):
                p = image_rgb.getpixel((x, y))
                colors_in_image.add(p)
                if p == color:
                    new_image.putpixel((x, y), 0)
                else:
                    new_image.putpixel((x, y), 1)

        images_by_color.append(new_image)
    
    logging.info(colors_in_image)
    return images_by_color

def write_images_by_color(images_by_color):
    for i, image in enumerate(images_by_color):
        image.save(f"image_{i}.bmp", format="BMP")

def refresh_screen(images_by_color, first_loop, configuration):
    try:
        logging.info("Refresh the eink")
        if configuration['display']['type'] == "eink_2in7":
            epd = epd2in7b.EPD()
        elif configuration['display']['type'] == "eink_7in3":
            epd = epd7in3g.EPD()
        elif configuration['display']['type'] == "png":
            images_by_color[0].save("output.png")
            return
        else:
            logging.info("Skip refresh screen")
            return

        logging.info("init and Clear")
        epd.init()
        if first_loop == True:
            epd.Clear()
        
        logging.info("Prepare image for the screen")

        buffers = [epd.getbuffer(ImageOps.flip(ImageOps.mirror(ci))) for ci in images_by_color]
        epd.display(*buffers)
        
        logging.info("Goto Sleep...")
        epd.sleep()
            
    except IOError as e:
        logging.info(e)

def main_loop():
    logging.getLogger().setLevel(level=logging.DEBUG)
    logging.info("Start.")
    logging.info("Wait for internet connection.")

    if supportForEink==True:
        logging.info("Enable eink support")
        configuration['display'] = configurations_display['eink_7in3'] 
    else:
        logging.info("Disable eink support")
        configuration['display'] = configurations_display['png']
    
    previous_calendar_data = None
    needsRefresh = False
    lastRefresh = datetime.datetime.min
    ical_link = ICAL_URL
    first_loop = True
    images_by_color = None
    while True:
        logging.info("Loop. Try to download ical data")
        while check_internet() == False:
            logging.info("No internet connection.")
            time.sleep(5)
        calendar_data = download_calendar_data(ical_link, configuration)

        if calendar_data != None and calendar_data != previous_calendar_data:
            previous_calendar_data = calendar_data
            needsRefresh = True

        if calendar_data != None and needsRefresh == True:
            logging.info(f'Process calendar data')
            calendar_data_processed = {}
            calendar_data_processed['tasks_by_day']= process_calendar_data(calendar_data, configuration)

            calendar_data_processed['current_day_nr'] = datetime.date.today().weekday()
            calendar_data_processed['current_day_name'] = configuration['days_names'][datetime.date.today().weekday()]
            calendar_data_processed['current_date'] = datetime.datetime.now().strftime(r'%d-%m-%Y %H:%M:%S')

            other_files_paths = ['fonts.css', 'unscii-8.woff']
            html_data = generate_html(calendar_data_processed, configuration)
            print(html_data)
            image = generate_rgb_image(html_data, other_files_paths, configuration)

            # images_by_color = split_image_by_color(image, colors=[ImageColor.getrgb("#000000"), ImageColor.getrgb("#FFFFE0"), ImageColor.getrgb("#FFFFF0")])
            images_by_color = [image]
            write_images_by_color(images_by_color)

        if images_by_color != None and (needsRefresh==True or datetime.datetime.now() >= lastRefresh + datetime.timedelta(minutes=configuration['refresh_minutes_max'])):
            logging.info("Refresh.")
            lastRefresh = datetime.datetime.now()
            refresh_screen(images_by_color, first_loop, configuration)
            first_loop = False
        
        time.sleep(configuration['refresh_minutes_min']*60)


if __name__ == "__main__":
    main_loop()

    
